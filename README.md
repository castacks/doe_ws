# DOE Raspberry Pi Workspace
Workspace for recording data on the Raspberry Pi

## Installation  
- Clone Repository and run ```wstool init src install.rosinstall```  
- Run ```catkin build``` in the main folder

## Recording Data
Use the ```record.sh``` script to record data.

## Core Contributors
Jay Patrikar (jaypat@cmu.edu)

Bastian Wagner

## Maintaner
Jay Patrikar (jaypat@cmu.edu)

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)
 
Copyright (c) 2020, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
